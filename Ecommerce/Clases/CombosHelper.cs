﻿using Ecommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace Ecommerce.Clases
{
    public class CombosHelper : IDisposable
    {
        private static EcommerceContext db = new EcommerceContext();
        public static List<Department> GetDepartments()
        {
            var departments = db.Departments.OrderBy(d => d.Name).ToList();
            departments = departments.OrderBy(d => d.Name).ToList();
            departments.Insert(0, new Department
            {
                DepartmentId = 0,
                Name = "Seleccione un Departamento"
            });

            return departments;
        }

        public static List<Product> GetProducts(int companyId)
        {
            var products = db.Products.Where(p => p.CompanyId == companyId).ToList();
            products = products.OrderBy(c => c.Description).ToList();
            products.Insert(0, new Product
            {
                ProductId = 0,
                Description = "Seleccione un Producto"
            });
            return products;
        }

        public static List<City> GetCities()
        {
            var cities = db.Cities.OrderBy(d => d.Name).ToList();
            cities = cities.OrderBy(d => d.Name).ToList();
            cities.Insert(0, new City
            {
                CityId = 0,
                Name = "Seleccione una Ciudad"
            });

            return cities;
        }

        public static List<Company> GetCompanies()
        {
            var companies = db.Companies.OrderBy(d => d.Name).ToList();
            companies = companies.OrderBy(d => d.Name).ToList();
            companies.Insert(0, new Company
            {
                CompanyId = 0,
                Name = "Seleccione una Compañia"
            });

            return companies;
        }

        public static List<Customer> GetCustomers(int companyId)
        {
            var customers = db.Customers.Where(c => c.CompanyId == companyId).ToList();
            customers = customers.OrderBy(c => c.FirstName).ThenBy(c=>c.LastName).ToList();
            customers.Insert(0, new Customer
            {
                CustomerId = 0,
                FirstName = "Seleccione un Cliente"
            });
            return customers;
        }

        public static List<Category> GetCategories(int companyId)
        {
            var categories = db.Categories.Where(c => c.CompanyId == companyId).ToList();
            categories = categories.OrderBy(c => c.Description).ToList();
            categories.Insert(0, new Category
            {
                CategoryId = 0,
                Description = "Seleccione una Categoría"
            });
            return categories;
        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = db.Taxes.Where(t => t.CompanyId == companyId).ToList();
            taxes = taxes.OrderBy(t => t.Description).ToList();
            taxes.Insert(0, new Tax
            {
                TaxId = 0,
                Description = "Seleccione un Impuesto"
            });
            return taxes;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}