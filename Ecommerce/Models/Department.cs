﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage ="El campo {0} es requerido")]
        [MaxLength(50,ErrorMessage ="El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name="Departamento")]
        [Index("Department_Name_Index",IsUnique = true)]
        public string Name { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<Company> Companies { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<User> Warehouses { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}