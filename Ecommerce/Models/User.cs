﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(256, ErrorMessage = "El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name = "Correo Electrónico")]
        [Index("User_UserName_Index",IsUnique =true)]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }


        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name = "Primer Nombre")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(20, ErrorMessage = "El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name = "Teléfono")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [MaxLength(100, ErrorMessage = "El campo{0} debe ser como máximo de {1} caracteres")]
        [Display(Name = "Dirección")]
        public string Adress { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Photo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        [Display(Name ="Departamento")]
        public int DepartmentId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        [Display(Name = "Ciudad")]
        public int CityId { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        [Display(Name = "Compañia")]
        public int CompanyId { get; set; }

        [NotMapped]
        [Display(Name ="Usuario")]
        public string FullName { get { return string.Concat(FirstName, " ", LastName); } }

        [NotMapped]
        [Display(Name ="Seleccione Foto")]
        public HttpPostedFileBase PhotoFile { get; set; }

        public virtual Department Department { get; set; }
        public virtual  City City { get; set; }
        public virtual Company Company { get; set; }
    }
}